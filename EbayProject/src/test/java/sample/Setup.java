package sample;

import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

import org.testng.annotations.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class Setup {
	
	WebDriver driver;
  @Test
  public void f() throws InterruptedException {
	  driver.get("https://www.ebay.com/");
	  Thread.sleep(2000);
	  System.out.println(driver.getTitle());
  }
  @BeforeClass
  public void beforeClass() {
	  WebDriverManager.chromedriver().setup();
	  driver=new ChromeDriver();
	  
  }

  @AfterClass
  public void afterClass() {
	  driver.close();
  }

}
